class CreateTorneos < ActiveRecord::Migration[5.1]
  def change
    create_table :torneos do |t|
      t.string :evento
      t.string :nombre
      t.string :equipos_participantes
      t.string :localizacion
      t.string :organizador

      t.timestamps
    end
  end
end
