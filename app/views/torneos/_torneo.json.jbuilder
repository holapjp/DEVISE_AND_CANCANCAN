json.extract! torneo, :id, :evento, :nombre, :equipos_participantes, :localizacion, :organizador, :created_at, :updated_at
json.url torneo_url(torneo, format: :json)
