class TorneosController < ApplicationController
  before_action :set_torneo, only: [:show, :edit, :update, :destroy]
  layout "salir"

  # GET /torneos
  # GET /torneos.json
  def index
    @torneos = Torneo.all
  end

  # GET /torneos/1
  # GET /torneos/1.json
  def show
    @torneos = Torneo.find(params[:id])
    authorize! :read, @torneos
  end

  # GET /torneos/new
  def new
    @torneo = Torneo.new
  end

  # GET /torneos/1/edit
  def edit
  end

  # POST /torneos
  # POST /torneos.json
  def create
    @torneo = Torneo.new(torneo_params)

    respond_to do |format|
      if @torneo.save
        format.html { redirect_to @torneo, notice: 'Torneo was successfully created.' }
        format.json { render :show, status: :created, location: @torneo }
      else
        format.html { render :new }
        format.json { render json: @torneo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /torneos/1
  # PATCH/PUT /torneos/1.json
  def update
    respond_to do |format|
      if @torneo.update_attributes(uptade_params)
        format.html { redirect_to @torneo, notice: 'Torneo was successfully updated.' }
        format.json { render :show, status: :ok, location: @torneo }
      else
        format.html { render :edit }
        format.json { render json: @torneo.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update_params
    params.require(:article).permit(:body)
  end

  # DELETE /torneos/1
  # DELETE /torneos/1.json
  def destroy
    @torneo.destroy
    respond_to do |format|
      format.html { redirect_to torneos_url, notice: 'Torneo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_torneo
      @torneo = Torneo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def torneo_params
      params.require(:torneo).permit(:evento, :nombre, :equipos_participantes, :localizacion, :organizador)
    end
    
    def create_params
    
    end 
end
