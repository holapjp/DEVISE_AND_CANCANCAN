class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true;
  before_action :authenticate_user!
  before_action :user_signed_in?
  before_action :current_user
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
end
